public class Dog extends Animal{
    private boolean cuteSnout;

    public Dog(int id, String name, String favFood, boolean cuteSnout){
        super(id, name, favFood);
        this.cuteSnout = cuteSnout;
    }

    public void setCuteSnout(boolean cuteSnout){
        this.cuteSnout = cuteSnout;
    }

    public boolean getCuteSnout(){
        return this.cuteSnout;
    }

    // method overriding
    public String details(){
        return super.details() + " and " +
            ((this.cuteSnout) ? "has " : "does not have ") + "cute snout";
    }
    public void makeSound(){
        System.out.println("bow wow");
    }

    // method overloading
    public void makeSound(String sound, int times){
        for(int i = 0; i < times; i++ ){
            System.out.print("woof woof ");
        }
        System.out.println();
    }
}