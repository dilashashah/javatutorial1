public class Animal{
    private int id;
    private String name;
    private String favFood;

    public Animal(int id, String name, String favFood){
        this.id = id;
        this.name = name;
        this.favFood = favFood;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setFavFood(String favFood){
        this.favFood = favFood;
    }
    
    public String getName(){
        return this.name;
    }
    public String getFavFood(){
        return this.favFood;
    }

    public void makeSound(){
        System.out.println("grrrrrr");
    }
    public void sleep(){
        System.out.println("zzzzzz");
    }

    public String details(){
        return this.name + " loves to eat " + this.favFood;
    }
}