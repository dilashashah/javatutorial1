public class Test{
    public static void main (String[] args){
        Animal zaara = new Animal(1, "Zaara", "meat");
        System.out.println(zaara.getName() + " loves to eat" + zaara.getFavFood());

        Animal rio = new Animal(2, "Rio", "mango");
        System.out.println(rio.getFavFood());
        rio.setFavFood("orange");
        System.out.println(rio.getFavFood());
        rio.makeSound();

        Dog bhotu = new Dog(3, "Bhotu", "roti", true);
        System.out.println(bhotu.getName() + " loves to eat " + bhotu.getFavFood() +
        " and " + ((bhotu.getCuteSnout()) ? "has " : "does not have ") + "cute snout");
        bhotu.makeSound("boof", 4);

        Animal cosmo = new Dog(4, "Cosmo", "bread and jam", true);
        System.out.println(cosmo.details());


        

    }
}